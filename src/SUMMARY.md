# Summary

- [Introduction](./introduction.md)

# Getting started

- [Installation](./installation.md)
- [Editor Setup](./editor-setup.md)

# The Spade language

- [Blinky](./blinky.md)
  - [Blinky (for software people)](./blinky_sw.md)
  - [Blinky (for hardware people)](./blinky_hw.md)
- [Common Language Constructs](./common_language_constructs.md)
    - [Basic Expressions](./expressions.md)
    - [Spicy Expressions](./spicy_expressions.md)
    - [Variables](./variables.md)
    - [Units](./units.md)
- [Pipelines](./pipelines.md)
    - [Pipelines (for software people)](./pipelines_sw.md)
    - [Pipelines (for hardware people)](./pipelines_hw.md)
- [Simulation and Testing](./simulation.md)
- [Ports and Wires](./wires.md)
- [Interfacing with Verilog](./external_verilog.md)

# Example projects

- [Sample project: ws2128 RGB LED driver](./ws2812-example/index.md)
  - [Creating a Project](./ws2812-example/project.md)
  - [Led Protocol Overview](./ws2812-example/led_protocol_overview.md)
  - [Driver Interface](./ws2812-example/driver_interface.md)
  - [State Machine](./ws2812-example/state_machine.md)
  - [Testing the State Machine](./ws2812-example/testing_the_state_machine.md)
  - [Output generation](./ws2812-example/output_generation.md)
  - [Testing in hardware](./ws2812-example/testing_in_hardware.md)

# Language & Tool References

- [Language Reference](./language_reference/index.md)
  - [Items](./language_reference/items.md)
    - [Units](./language_reference/items/units.md)
    - [Type Declarations](./language_reference/items/type_declarations.md)
  - [Statements](./language_reference/statements.md)
  - [Expressions](./language_reference/expressions.md)
    - [Blocks](./language_reference/expressions/block.md)
    - [If](./language_reference/expressions/if.md)
    - [Match](./language_reference/expressions/match.md)
    - [Instantiation](./language_reference/expressions/instantiation.md)
    - [Indexing](./language_reference/expressions/indexing.md)
    - [Stage References](./language_reference/expressions/stage_reference.md)
  - [Patterns](./language_reference/pattern.md)
    - [Refutability](./language_reference/pattern/refutability.md)
  - [Type system](./language_reference/type_system/index.md)
    - [Primitive Types](./language_reference/type_system/primitives.md)
    - [Generics](./language_reference/type_system/generics.md)
    - [Ports and Wires](./language_reference/type_system/wires.md)
  - [Dynamic Pipelines](./language_reference/dynamic_pipelines.md)
  - [Binding](./binding.md)
  - [Constructs by syntax](./language_reference/constructs_by_syntax.md)

- [Swim](./swim/index.md)
  - [Installing Swim](./swim/install.md)
  - [Using Swim](./swim/usage.md)
  - [Custom Subcommands](./swim/subcommands.md)
  - [Project Configuration](./swim_project_configuration/config__Config.md)
  - [Plugin Configuration](./swim_project_configuration/plugin__config__PluginConfig.md)

---

- [Compiler Internals](./internal/index.md)
  - [Naming](./internal/naming.md)
  - [Type Representation](./internal/type_representations.md)
