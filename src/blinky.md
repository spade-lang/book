# Blinky

The traditional program to start learning any language is "hello, world!".
However, printing a string in hardware is a complex task, so the "hello,
world!" in hardware is usually blinking an LED.

This chapter has two versions:
 - [Blinky (for hardware people)](./blinky_hw.md) for people who have some experience with digital hardware and want to
   learn Spade. This version focuses on the syntax of the language and makes
   comparisons to Verilog and VHDL, but assumes some familiarity with things like
   registers.
- [Blinky (for software people)](./blinky_sw.md) for people who are used to
  software development but are new to hardware.
  This version puts less emphasis on the syntax of the language, and more on the
  basic hardware it is describing.
