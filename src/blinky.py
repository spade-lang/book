
COUNTER = 0

def blinky():
  global COUNTER
  if COUNTER == 1000:
    COUNTER = 0
  else:
    COUNTER = COUNTER + 1
  # The LED should be on in the second counter interval
  return COUNTER > 500

import time

while True:
  start = time.time()
  print(blinky())
  end = time.time()
  time.sleep(0.001 - (end - start))
