# Blinky (for software people)


This chapter will show the very basics of Spade and is aimed at people who are
familiar with software development but are new to hardware. If you come here
with some experience in hardware design with VHDL or Verilog, the [Blinky (for
hardware people)](./blinky_hw.md) chapter is probably more useful.

Before blinking an LED, we can try turning on or off an LED. We can do this as
```spade
{{ #include swim_test_project/src/blinky.spade:led_on }}
```
To Rust users, this will likely feel very familiar. To those familiar with
other languages, the last value at the end of a block is "returned", so this is
an `entity` which returns `true`.
If we connect the output signal of this to an LED, it would turn on. If you're curious, you can try it [▶️ on the playground][constant_led]

This isn't particularly interesting though, so let's do something more
interesting. Blinking an LED is the typical "Hello, World" in hardware, but
even that requires some complexity so we will build up to it. Let's first start
by making the LED turn off while we hold down a button, which first requires
taking a `btn` as an input:

```spade
{{ #include swim_test_project/src/blinky.spade:led_btn_input }}
```
and then changing the output to `!btn`
```spade
{{ #include swim_test_project/src/blinky.spade:led_button }}
```

If you [▶️ try this][try_button_link], you can see that if you press the button,
the LED turns off, and if you release it, it will turn on again. Here we're
just simulating the resulting hardware, but if we connected this up to real
hardware, it would also work!

If you think about this for a while, you may start wondering _when_ this gets
"evaluated". In software, this "function" would be called once, giving it the value of
the button and generating a single result. But this somehow reacts to inputs!
While Spade, and many HDLs for that matter may look like software, it is
important to note that we are _not_ describing instructions for some processor
to execute, we are describing hardware to be built on a chip. The code we
wrote says "connect input `btn` to an inverter, whose output in turn should be
connected to the output of the module which we externally connect to an LED.

If we want to approximate the behaviour from a software perspective, we can
view the progrmaming model of Spade either as continuously re-evaluating every
value in the design, or as re-evaluating every value when the values it depends
on changes.

At this point, we can start thinking about actually making an LED blink. In software we'd probably accomplish this by writing something along the lines of
```python
def main():
  led_on = Talse
  while True:
    led_on = not led_on;
    set_led(led_on);
    sleep(0.5);
```
However, because we are describing hardware, not software we can't really
"loop". Every expression we write will correspond to some physical block of
hardware, rather than instructions that get executed.

## A Detour Over Software

Before talking about how we would make a LED blink in hardware and Spade, it is helpful to
talk about how we might write a software function to to "blink" an LED if we
can't have loops inside our function. Remember that we can view our execution
model as constantly re-evaluating our function to get its new values, roughly
```python
def blinky():
  return True

while True:
  print(blinky())
```

On the surface, it might seem very difficult to make this thing blink, but if
we had some way to maintain state between calls of the function. In software,
we can achieve this by using a global variable for the state of the LED
```python
LED_ON = False
def blinky():
  global LED_ON
  LED_ON = not LED_ON
  return LED_ON


while True:
  print(blinky())
```
If we run this program, we'll now get alternating `True` and `False`
```
True
False
True
False
...
```

There are some problems with this though, our value is "blinking" far too fast
for us to see it blinking. If this were hardware, the LED would just look dim
as opposed to clearly switching between on and off, we need to regulate it
somehow. A quick way to do this would be to just call our function less often,
for example, once per second. As we'll see, this is something we can kind of do in
hardware, so let's try it!

```python
import time

while True:
  start = time.time()
  print(blinky())
  end = time.time()
  # We want each iteration to take 0.5 seconds
  # so we get a blinking frequency of 1 hz.
  # To avoid drifting if `blinky` ends up taking
  # a long time, we'll compute how long the evaluation
  # took and subtract that from the period
  time.sleep(0.5 - (end - start))
```

That works, but has a major problem: now we cannot do anything more often than
once per second, so if our program was to do more things than blinking an LED,
we're probably screwed. To solve this, we can reduce the sleep time to
something faster, but which we can still manage without having `end-start` become larger than the period. Being conservative, we'll aim for a frequency of `1 KHz`
```python
import time

while True:
  start = time.time()
  print(blinky())
  end = time.time()
  time.sleep(0.001 - (end - start))
```
If we just run our blinky now, we're back to it blinking faster than we can see, so we'll
need to adjust it to compute how long it has been running and toggling the LED accordingly
```python
COUNTER = 0
def blinky():
  global COUNTER
  if COUNTER == 1000:
    COUNTER = 0
  else:
    COUNTER = COUNTER + 1
  # The LED should be on in the second counter interval
  return COUNTER > 500

import time

while True:
  start = time.time()
  print(blinky())
  end = time.time()
  time.sleep(0.001 - (end - start))
```

## Back To Hardware

At this point, you have got a sense of a (pretty cursed) programming model that approximates
hardware pretty well, so we can get back to writing hardware.

Almost all primitive hardware blocks are _pure_ (or combinatorial as it is
known in hardware). They take their inputs and produce an output. This includes
arithmetic operators, comparators, logic gates and "if expressions"
(multiplexers). Using these to build up any form of state, like our counter, will be very difficult.
Luckily there is a special kind of hardware unit called a _flip_flop_ which can remember a single
bit value. These come in several flavours and by far the most common is the D-flipflop which has
a signature that is roughly
```spade
entity dff(clk: clock, new_value: bool) -> bool
```
Its behaviour when the clock signal (`clk`) is unchanged is to simply remember
its current value. Flip flops become much more interesting when we start
toggling the clock. Whenever the `clk` signal changes from 0 to 1, it will
replace its currently stored value with the value that is on its new_value
input.

Hardware is often shown graphically, and a `dff` is usually drawn like this:

![src/dff_schematic.svg](images/dff_schematic.svg)

Using this, we can build our initial very fast blinking circuit like this:
```spade
{{ #include swim_test_project/src/blinky.spade:blinky_dff }}
```
Don't worry too much about the syntax here, we define `led_on` as a `dff`
whose new value is `!led_on`. When the `clk` goes from 0 to 1, the `dff` will
take the value that is on its input (`!led_on`) and set it as its internal
value, which makes the LED blink. This might be easier to understand graphically:

![A graphical representation of a circuit that toggles an LED on and off](images/fast_blinky_schematic.svg)

We can also visualize the value of the signals in the circuit over time, which
looks roughly like ![](images/blinking_pattern.svg)

As soon as the clock switches from 0 to 1, the value of `led_on` switches to
`new_value`. This in turn makes the output of the inverter change to the
inverse which is now the "new `new_value`". Then nothing happens until the
clock toggles again at which point the cycle repeats.

At this point, you should be wondering what the initial state of the register is as right now it only depends on itself. While it is possible to specify initial values in registers in FPGAs, that's not possible when building dedicated hardware, so the proper approach is to use a third input to the DFF that we left out for now: the reset. It takes a `bool` which tells the flip flop to reset its current value if 1, and a value to reset to. Again, looking at the signature, this would be roughly
```spade
entity dff(clk: clock, rst_trigger: bool, initial_value: bool, new_value: bool) -> bool
```
When `rst` is true, the internal value of the dff will get set to `initial_value`.

Visualized as signal values over time, this looks like:
![](images/blinking_with_reset.svg)

The `clk` and `rst_trigger` signal are typically fed to our hardware
externally. The clock is as you may expect from reading clock signal
specifications on hardware, quite fast. Not quite the 3-5 GHz that you may
expect from a top of the line processor, but usually between 10 500 MHz in
FPGAs. This means that we need to pull the same trick we did in our software
model to make the blinking visible: maintain a counter of the current time and
use that to derive if the led should be on or not.

Our counter needs to be quite big to count on human time scales with a 10 Mhz
clock, so building a counter from individual `bool`s with explicit `dff`s for
each of them is infeasible. Therefore, we almost always use "registers" for our state. These are just banks of `dff` with a shared clock and reset.

Additionally, using our `dff` entity isn't super ergonomic since it requires that `decl` keyword, so Spade has dedicated syntax for registers. It looks like this
```spade
{{ #include swim_test_project/src/blinky.spade:reg_sample }}
```
which, admittedly is quite dense syntax. It helps to break it down in pieces though
- `reg(clk)` specifies that this is a register that is clocked by `clk`.[^multi_clock]
- `value` is the name of the variable that will hold the current register value
- `: uint<8>` specifies the type of the register, in this case an 8 bit
  unsigned value. In most cases, the type of variables can be inferred, so this
  can be left out
- `reset(rst: reset_value)` says that the register should be set back to
  `reset_value` when `rst` is true. If the register does not depend on itself, it
  can be omitted

## Blinky, Finally

We _finally_ have all the background we need to *drumroll* 🥁 blink an LED! The code to
do so looks like this
```spade
{{ #include swim_test_project/src/blinky.spade:full_example }}
```
Looking at the python code we wrote before, we can see some similarities. Our
global `count` has been replaced with a `reg`. `reg` has a special scoping rule
that allows it to depend on its own value, unlike normal `let` bindings which
are used to define other values. The new value of the register is given in
terms of its current value. If it is `duration` , it is set to 0, otherwise it
is set to `count + 1`.

`trunc` is needed since Spade prevents you from overflows and underflows by
extending signals when they have the potential to overflow. `count + 1` can
require one more bit than `count`, so you need to explicitly convert the value
down to `28` bits. `trunc` is short for "truncate" which is the hardware way of
saying "throwing away bits".

Those unfamiliar with Rust or other functional languages may be a bit surprised that the `if` isn't written as
```spade
if count == duration {
  count = 0
} else {
  count = trunc(count + 1)
}
```
This is because Spade is _expression based_ -- conditional return values
instead of having side effects. This is because in hardware, we can't really
re-assign a value conditionally, the input to the "new value" field of the
register is a single signal, so all variables in Spade are immutable.

If you are used to C or C++, you can view `if` expressions as better ternary
operators (`cond ? on_true : on_false`), and python users may view them as the
`on_true if cond else false` construct.

## Play around

At this point it might be fun to play a little bit with the language, you
could try modifying the code to:

- Add an additional input to the `entity` called `btn` which can be used to pause the counter
- Use `btn` to invert the blink pattern

You can try the code directly in your browser at [▶️ play.spade-lang.org][play_blinky]

[^several_blocks]: Technically, there are a whole family, but in practice we
almost always use registers built from D-flip flops.
[^multi_clock]: Most of the time when starting out you'll just have one clock, but as you build bigger systems, you'll eventually need multiple clocks


[try_button_link]:https://play.spade-lang.org/#eyJzcGFkZVNvdXJjZSI6ImVudGl0eSBibGlua3koY2xrOiBjbG9jaywgcnN0OiBib29sLCBidG46IGJvb2wpIC0+IGJvb2wge1xuICAhYnRuXG59XG5cbi8vIEluIG9yZGVyIHRvIGludGVyZmFjZSB3aXRoIHRoZSBleHRlcm5hbCB3b3JsZCwgYm90aCB0aGUgc2ltdWxhdG9yIGhlcmVcbi8vIGFuZCBhY3R1YWwgaGFyZHdhcmUsIHdlIG5lZWQgdG8gY2hhbmdlIHRoZSBzaWduYXR1cmUgb2YgdGhlIGVudGl0eSBhIGJpdFxuLy8gSWYgeW91J3JlIGp1c3Qgc3RhcnRpbmcgb3V0LCBkb24ndCB3b3JyeSBhYm91dCB0aGlzIHBhcnQhXG4jW25vX21hbmdsZV1cbmVudGl0eSB0b3AoI1tub19tYW5nbGVdIGNsazogY2xvY2ssICNbbm9fbWFuZ2xlXSBsZWQ6IGludiAmIGJvb2wsICNbbm9fbWFuZ2xlXSBidG46IGJvb2wpIHtcbiAgcmVnKGNsaykgcnN0IGluaXRpYWwodHJ1ZSkgPSBmYWxzZTtcblxuICBzZXQgbGVkID0gaW5zdCBibGlua3koY2xrLCByc3QsIGJ0bik7XG59XG4iLCJ0b21sU291cmNlIjoibmFtZSA9IFwic3dpbV9ibGlua3lcIlxuIiwic2ltS2luZCI6IkxFRCJ9

[constant_led]:https://play.spade-lang.org/#eyJzcGFkZVNvdXJjZSI6ImVudGl0eSBibGlua3koY2xrOiBjbG9jaywgcnN0OiBib29sKSAtPiBib29sIHtcbiAgdHJ1ZVxufVxuXG4vLyBJbiBvcmRlciB0byBpbnRlcmZhY2Ugd2l0aCB0aGUgZXh0ZXJuYWwgd29ybGQsIGJvdGggdGhlIHNpbXVsYXRvciBoZXJlXG4vLyBhbmQgYWN0dWFsIGhhcmR3YXJlLCB3ZSBuZWVkIHRvIGNoYW5nZSB0aGUgc2lnbmF0dXJlIG9mIHRoZSBlbnRpdHkgYSBiaXRcbi8vIElmIHlvdSdyZSBqdXN0IHN0YXJ0aW5nIG91dCwgZG9uJ3Qgd29ycnkgYWJvdXQgdGhpcyBwYXJ0IVxuI1tub19tYW5nbGVdXG5lbnRpdHkgdG9wKCNbbm9fbWFuZ2xlXSBjbGs6IGNsb2NrLCAjW25vX21hbmdsZV0gbGVkOiBpbnYgJiBib29sLCAjW25vX21hbmdsZV0gYnRuOiBib29sKSB7XG4gIHJlZyhjbGspIHJzdCBpbml0aWFsKHRydWUpID0gZmFsc2U7XG5cbiAgc2V0IGxlZCA9IGluc3QgYmxpbmt5KGNsaywgcnN0KTtcbn1cbiIsInRvbWxTb3VyY2UiOiJuYW1lID0gXCJzd2ltX2JsaW5reVwiXG4iLCJzaW1LaW5kIjoiTEVEIn0=

[play_blinky]:https://play.spade-lang.org/#eyJzcGFkZVNvdXJjZSI6ImVudGl0eSBibGlua3koY2xrOiBjbG9jaywgcnN0OiBib29sLCBidG46IGJvb2wpIC0+IGJvb2wge1xuICAvLyBGb3IgcGVyZm9ybWFuY2UgcmVhc29ucywgdGhlIExFRCBzaW11bGF0aW9uIGlzIGNhcHBlZCB0byAxMCBLSHosIHNvIHdlIHVzZVxuICAvLyBhIGxvd2VyIGR1cmF0aW9uIHRoYW4gd2Ugd291bGQgaW4gaGFyZHdhcmUgd2hlcmUgdGhpbmdzIHdvdWxkIHJ1biBhdCA+MTAgTUh6XG4gIC8vIG1vc3Qgb2YgdGhlIHRpbWVcbiAgbGV0IGR1cmF0aW9uID0gMTBfMDAwO1xuICByZWcoY2xrKSBjb3VudDogdWludDwxNT4gcmVzZXQocnN0OiAwKSA9XG4gICAgaWYgY291bnQgPT0gZHVyYXRpb24ge1xuICAgICAgMFxuICAgIH0gZWxzZSB7XG4gICAgICB0cnVuYyhjb3VudCArIDEpXG4gICAgfTtcblxuICBjb3VudCA+IChkdXJhdGlvbiAvIDIpXG59XG5cbi8vIEluIG9yZGVyIHRvIGludGVyZmFjZSB3aXRoIHRoZSBleHRlcm5hbCB3b3JsZCwgYm90aCB0aGUgc2ltdWxhdG9yIGhlcmVcbi8vIGFuZCBhY3R1YWwgaGFyZHdhcmUsIHdlIG5lZWQgdG8gY2hhbmdlIHRoZSBzaWduYXR1cmUgb2YgdGhlIGVudGl0eSBhIGJpdFxuLy8gSWYgeW91J3JlIGp1c3Qgc3RhcnRpbmcgb3V0LCBkb24ndCB3b3JyeSBhYm91dCB0aGlzIHBhcnQhXG4jW25vX21hbmdsZV1cbmVudGl0eSB0b3AoI1tub19tYW5nbGVdIGNsazogY2xvY2ssICNbbm9fbWFuZ2xlXSBsZWQ6IGludiAmIGJvb2wsICNbbm9fbWFuZ2xlXSBidG46IGJvb2wpIHtcbiAgcmVnKGNsaykgcnN0IGluaXRpYWwodHJ1ZSkgPSBmYWxzZTtcblxuICBzZXQgbGVkID0gaW5zdCBibGlua3koY2xrLCByc3QsIGJ0bik7XG59XG4iLCJ0b21sU291cmNlIjoibmFtZSA9IFwic3dpbV9ibGlua3lcIlxuIiwic2ltS2luZCI6IkxFRCJ9
