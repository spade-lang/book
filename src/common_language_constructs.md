# Common Language Constructs

This chapter goes through common constructs that most languages have such as
variables, expressions basic types, and conditionals. The focus is on how they work
in Spade and how that is different from other languages.
