# Editor Setup

> Before installing locally, there is a "playground" available at
> [▶️ play.spade-lang.org](https://play.spade-lang.org) which you can use to play around
> with the language. The first few chapters of the book use that, so if you want to follow
> along with the tutorial, you can skip this chapter until prompted to install Spade locally.

There are a variety of third-party plugins integrating Spade in different
editors.

## Vim

If you use Neovim, you can use [spade.nvim](https://github.com/ethanuppal/spade.nvim), which is maintained by Ethan at that GitHub repository.
This plugin sets up syntax highlighting and LSP automatically along with some
other quality-of-life features.

Otherwise, you can use <https://gitlab.com/spade-lang/spade-vim>, following the
instructions at that repository for manual setup.

## Vscode

- <https://git.sr.ht/~acqrel/spade-vscode>

## Emacs

- <https://git.sr.ht/~lucasklemmer/spade-mode>

### Other Editors

Made a plugin for your favorite editor?
[Submit a merge request](https://gitlab.com/spade-lang/book/-/merge_requests/new) to add it to this list!
