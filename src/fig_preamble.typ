#set text(size: 16pt)

#let stroke = 2pt + black

#let op(inner) = {
  circle(stroke: stroke, outset: -0.2em, inner)
}

#let reg(width: auto, name: none) = {
  let inset = 0.5em;
  let final_inset = (top: inset, left: inset*1.7, right: inset, bottom: inset)
  block(width: width, height: 1em, stroke: stroke, inset: final_inset, {
    box(polygon(
      stroke: stroke,
      (-inset*1.7 + 0em, -0.3em),
      (-inset*1.7 + 0.4em, -0.0em),
      (-inset*1.7 + 0em, 0.3em),
    ))
    box[#v(-0.3em) #name]
  })
}

#let mux(width: 5em, height: 1.2em) = {
  let slant = 0.7em
  polygon(
    stroke: 2pt + black,
    (0em, 0em),
    (width, 0em),
    (width - slant, height),
    (slant, height),
  )
}


// #op[$+$]
// #op[$>=$]
// #op[`+`]
// 
// #reg(width: 5em)
// 
// #reg(width: 5em, name: [`counter`])
// 
// #mux()
// 
// 
