# Installation

> Before installing locally, there is a "playground" available at
> [▶️ play.spade-lang.org](https://play.spade-lang.org) which you can use to play around
> with the language. The first few chapters of the book use that, so if you want to follow
> along with the tutorial, you can skip this chapter until prompted to install Spade locally.

At the moment, Spade works best on Linux systems, but macOS also works
quite well with only a few minor issues[^mac_issues]. Windows is not supported for now,
although it should be usable through WSL.

There are a few ways to start using Spade:

1. **Manually**

    In order to install Spade manually, you need the Rust toolchain, specifically `cargo`.
    If you don't have it installed, you can install it with <https://rustup.rs/>.
    Simply run the command there, make sure its binaries are in your `PATH`, and
    then run `rustup toolchain install stable`

    Unless you have specific needs, you should install the Spade compiler via its
    build tool Swim. Swim then manages the compiler version on a per-project basis.
    To install Swim, run

    ```bash
    cargo install --git https://gitlab.com/spade-lang/swim
    ```

2. **With a package manager**

    If you are on Arch Linux, you can install the `swim-git` package from the
aur <https://aur.archlinux.org/packages/swim-git>

3. **Using Docker**

    The [Spade Docker image](https://github.com/ethanuppal/spade-docker), which works on macOS as well, has all the necessary tooling and environment preconfigured.

    For example, here's how you would start an interactive shell where commands like
    `swim` are available:

    ```
    docker run -it --rm ghcr.io/ethanuppal/spade-docker:latest
    ```

    Make sure you have the Docker (or podman) daemon running in the background.
    Do note that the image only supports x86_64 and arm64.

You should now be able to create a swim project using `swim init hello_world`!

## Synthesis Tools and Simulators

Spade compiles to Verilog code which is simulated and synthesised (compiled to
hardware) by other tools — in particular, cocotb for simulation and yosys+nextpnr for synthesis.

### Automated install

The easiest way to install those tools is via Swim by running:

```bash
swim install-tools
```

which downloads [https://github.com/YosysHQ/oss-cad-suite-build](https://github.com/YosysHQ/oss-cad-suite-build) into
`~/.local/share/swim`. If it is installed, swim will always use the cad-suite tools instead of system tools.

> NOTE: If you need to uninstall those tools, remove `~/.local/share/swim/bin`

### Manual install

You can also install the tools manually. Refer to the individual installation instructions in that case. The tools you need are:

- **For simulation**:
  - Python3.8 or later
  - Simulation via Icarus Verilog (using cocotb): <https://github.com/steveicarus/iverilog>
  - Simulation via Verilator: <https://github.com/verilator/verilator>
- **For building for hardware**
  - Synthesis: <https://yosyshq.net/yosys/>
  - Place and route: <https://github.com/YosysHQ/nextpnr>. Build `nextpnr-ice40` or `nextpnr-ecp5` depending on your target FPGA
  - Bitstream generation and upload. Depends on your target FPGA

> If you're just starting out, you probably don't need all of these. Start of
> by simulating your designs using `cocotb` and `icarus`, then you can move on
> to testing in hardware using `yosys` and `nextpnr`.
>
> If your simulations are too slow, you can try `verilator`.

### Next steps

Now, move on to [setting up your editor](./editor-setup.md) to work with Spade.

[^mac_issues]: <https://gitlab.com/spade-lang/swim/-/issues?label_name=platform%3Amaos>
