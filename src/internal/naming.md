# Naming

This chapter describes the naming scheme used by the compiler when generating Verilog.
The goal of the Verilog generator is not to generate readable Verilog, but
there should be a clear two-way mapping between signal names in the source
Spade code and generated Verilog.
This mapping should be clear both to users reading lists of signals, for
example, in VCD files, and tools, for example VCD parsers.

## Variables

Because Spade does not have the same scoping rules as Verilog, some
deconfliction of names internal to a Verilog module is needed.

If a name `x` only occurs once in a unit, the corresponding Verilog name is `\x
`.
(This is using the Verilog raw escape string system, and some tools may
report the name as `x`).
If `x` occurs more than once, subsequent names are given an index ordered sequentially
in the order that they are visited during AST lowering[^1]
The `k`th occurrence of a name is suffixed by `_n{k}`

Pipelined versions of names are suffixed with `_s{n}` where `n` is the absolute
stage index of the stage.

Names of port type with mutable wires have an additional variable for the mutable
bits. This follows the same naming scheme as the forward name, but is suffixed by
`_mut`

The following is an example of the naming scheme
```spade
pipeline(1) pipe(
    x: bool, // "\x "
    y: (&bool, inv &bool) // "\y ", "y_o "
) {
        if true {
            let x = true; // "x_n1"
        } else {
            let x = false; // "x_n2"
        }
        let x = true; // "x_n3"
    reg; // "\x_s1 ", "x_n3_s1
        let z = true; // "\z "
}

```

Spade makes no guarantees about name uniqueness between generated Verilog modules.

[^1]: This is currently the lexical order of the occurrences, i.e. names which
    occur early in the module are given lower indices.
