# Type Representation

Description of the Verilog representation of Spade types

## Mixed-direction types

Types with mixed direction wires are split in two separate variables, typically `<name>` and `<name>_mut`. The structure of the forward part is the same as if the backward part didn't exist, and the backward part is structured as if it were the forward part.

For example, `(int<8>, inv &int<9>, int<2>, &mut int<3>)` is stored as `(int<8>, int<2>)` and `(int<9>, int<3>)`.

## Tuples

Tuples are stored with their members packed side to side, with the 0th member on the left.

```spade
let x: (int<8>, int<2>, bool) = (a, b, c);
```
is represented as
```verilog
logic x[10:0];
assign x = {a,b,c};
```
Binary representation
```
aaaaaaaabbc
```

## Enums

Enums are packed with the leftmost bits being the discriminant and the remaining bits
being the payload. The payload is packed left-to-right, meaning that the rightmost bits
are undefined if a variant is smaller than the largest variant.

```spade
enum A {
    V1(a: int<8>),
    V2(b: int<2>),
    V3(c: bool)
}
```
```
    9 8 7 6 5 4 3 2 1 0
    t t p p p p p p p p
    -------------------
V1: 0 0 a a a a a a a a
V2: 0 1 b b X X X X X X
V3: 1 0 c X X X X X X X
```
