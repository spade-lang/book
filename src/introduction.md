# The Spade Book

<img class="noinvert" src="images/spadefish-white-outline-200.png">

_By Frans Skarman, with contributions from the community_

[Spade](https://spade-lang.org) is a [Rust](https://rust-lang.org)-inspired
[hardware description
language](https://en.wikipedia.org/wiki/Hardware_description_language).

Learn how to [install Spade](./installation.md) and [setup your editor](./editor-setup.md).
Here are some suggestions to get started:

- For a gentle introduction to Spade, there is a [(work in progress) tutorial](./blinky.md) that starts with the "Hello World" of hardware — _blinky_.

- If you want to jump straight into something more complex, there is a chapter on
[implementing a ws2128 RGB LED driver](./ws2812-example/index.md) in Spade.

If you are more interested in a reference of all constructs in the language, see the [language reference](./language_reference/index.md).

Also, if you have any questions or would like to discuss Spade with others, feel
free to join the [Discord community](https://discord.gg/YtXbeamxEX) or [Matrix
channel](https://matrix.to/#/#spade:matrix.org).

> Spade is a work in progress language and so is this documentation.
> Writing documentation is hard since it is hard to know what details are
> obvious and what things need more explanation. To make the documentation better, your feedback is
> invaluable so if you notice anything that
> is unclear or needs more explanation, please, reach out either via a [GitLab
> issue](https://gitlab.com/spade-lang/book/-/issues/new) <!-- TODO link to issue
> creation --> or on [Discord](https://discord.gg/YtXbeamxEX).
> <!-- TODO: Not everyone likes discord, I'd like to set up a matrix mirror -->

## Other resources

- [The Spade website](https://spade-lang.org)
- [Discord community](https://discord.gg/YtXbeamxEX)
- [This book's repository](https://gitlab.com/spade-lang/book.git)
- [Spade's repository](https://gitlab.com/spade-lang/spade.git)

## Chapters

- [Sample project: ws2128 RGB LED driver](./ws2812-example/index.md)
- [Language Reference](./language_reference/index.md)
- [Compiler Internals](./internal/index.md)
