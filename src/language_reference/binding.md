# Binding

When a *value* is given a name, it is called *binding* the value to the name. For example
```spade
let a = some_value;
```
*binds* `a` to `some_value`. It is usually fine to view *binding* as setting the value
of a *variable* to a value, though *variable* usually implies that variable
value can be changed later, which is not the case in Spade.

