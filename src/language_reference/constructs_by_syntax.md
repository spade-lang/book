# Constructs by syntax

This is a list of syntax constructs in the language with a link to a description
of the underlying language construct. The list is split in two: constructs which
[start with a keyword](#keywords) and [those which do not](#symbolic)

## Keywords

- `entity ...` [Entity definition](./items/units.md#entities)
- `enum ...` [Struct definition](./items/type_declarations.md#enum)
- `fn ...` [Function definition](./items/units.md#functions)
- `inst a(...) ...` [entity instantiation](./expressions/instantiation.md)
- `inst(<depth>) a(...) ...` [pipeline instantiation](./expressions/instantiation.md)
- `let` [let binding](./statements.md#let-bindings)
- `pipeline(<depth>) ...` [Pipeline definition](./items/units.md#pipelines)
- `reg(<clk>) ...;` [Register definition](./statements.md#registers)
- `reg;` [Pipeline stage marker](./statements.md#pipeline-stage-markers)
- `reg * <number>;` [Pipeline stage marker](./statements.md#pipeline-stage-markers)
- `reg[<condition>];` [Pipeline stage marker](./statements.md#pipeline-stage-markers)
- `struct ...` [Struct definition](./items/type_declarations.md#struct)

## Symbolic

- `<T>`, `<#N>` [Generic arguments](./type_system/generics.md)
- `a(...) ...` [function instantiation](./expressions/instantiation.md)
- `inst a(...) ...` [entity instantiation](./expressions/instantiation.md)
- `inst(<depth>) a(...) ...` [pipeline instantiation](./expressions/instantiation.md)
- `a[..]` [array indexing](./expressions/indexing.md)
- `a[..:..]` [array range indexing](./expressions/indexing.md)
- `a#..` [tuple indexing](./expressions/indexing.md)

