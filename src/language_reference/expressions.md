# Expressions

An expression is anything that has a value. Like most languages this includes
things like [integers literals](./expressions/literals.md),
[instantiations](./expressions/instantiation.md) and
[operators](./expressions/operators.md). However, unlike the languages you may
be used to, almost everything in Spade is an expression and has a value, for
example [if-expression](./expressions/if.md) and
[match-blocks](./expressions/match.md).

This means, among other things, that you can assign the 'result' of an
`if`-expression to a variable:

```spade
let a = if choose_b {
    b
}
else {
    c
};
```


