# Blocks

A block is an expression which can contain sub-[statements](../statements.md).
They are delimited by `{}`, contain zero or more [statements](../statements.md)
and *usually* end with an expression for the whole block's value.

```spade
let a = {
    let partial_result = ...; // Bind a partial result to a variable

    // 'return' the result of compute_on as the result of the block
    compute_on(partial_result)
};
```

Variables defined inside blocks are only visible in the block. For example, you cannot use `partial_result` outside the block above.

Blocks are required in places like bodies of `if`-expressions and functions,
but can be used in any place where an [expression](../expressions.md) is
expected.
