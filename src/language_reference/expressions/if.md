# `if`-expressions

## Syntax
> `if` [expression](../expressions.md) [block](./block.md) `else` [block](./block.md)

The `if`-expression looks a lot like an if-statement in languages you may be
used to, but unlike most languages where `if` is used to conditionally *do
something*, in Spade, it is used to *select* values.

For example, the following function returns `a` if `select_a` is true, otherwise it returns `b`.

```spade
fn select(select_a: bool, a: int<8>, b: int<8>) -> int<8> {
    if select_a {
        a
    } else {
        b
    }
}
```

This code makes heavy use of [blocks](./block.md). The body of the function,
as well as each if-branch is a block.

In traditional hardware description languages, this would instead look like
```
fn select(select_a: bool, a: int<8>, b: int<8>) -> int<8> {
    var result;
    if select_a {
        result = a;
    } else {
        result = b;
    }
    return result
}
```
but the Spade version is much closer to the actual hardware that is generated.
Hardware in general does not support conditional *execution*, it will evaluate
both branches and select the result.
