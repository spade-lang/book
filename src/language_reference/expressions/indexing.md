# Array Indexing

Arrays can be indexed using `[]`. Indices can either be single runtime integers
such as `[i]`, or compile-time ranges, such as `[10:15]`. Arrays are written
and indexed as most software languages: the leftmost array element is at index
0.

For example, `[a, b, c][0:2]` returns a new array `[a, b]`

## Single element indexing

Non-range indices can be runtime integers. The size of the index is the
smallest power of two that can represent the size of the array. However,
if the array size is not an even power of two, indexing outside the range causes
**undefined behavior**.

## Range indexing

The indices for range indexing can only be raw integers, i.e. not runtime values.
The leftmost, i.e. beginning of the range is included, and the end of the range
is exclusive. For example, `a[0:1]` creates an array with a single element
containing the first element of `a`.

## Examples

```spade
{{#include ../../swim_test_project/src/indexing.spade:example}}
```


# Tuple indexing

Tuples can also be indexed, though tuple indexing uses `#`, for example `tup#0`
for the leftmost tuple value. Tuple indices can only be known at compile time

