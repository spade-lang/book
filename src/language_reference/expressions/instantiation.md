# Instantiation

The [three kinds of units](../items/units.md) are instantiated in different ways in order to
highlight to readers of the code what might happen beyond an instantiation. For
example if you see a function instantiation, you know that there will be no
state or other weird behavior behind the instantiation.

The following syntax is used to instantiate the different kinds of units:

- Functions: `unit()`
- Entities: `inst unit()`
- Pipelines `inst(<depth>) unit()`. The depth is the depth of the pipeline

## Instantiation rules

Functions can be instantiated anywhere. Entities and pipelines can only be
instantiated in entities or pipelines.

In addition, pipelines instantiated in other pipelines check the delay to make sure
that values are ready before they are readable. For example,

```spade
    let x = inst(3) subpipe();
    let y = function();
reg;
    read(x); // Compilation error. x takes 3 cycles to compute, but is read after 1
    read(y); // Allowed, function is pure so its output is available immeadietly
reg * 2;
    // Allowed, x has 3 stages internally, this will be the first value out of the pipeline
    read(x)
```



