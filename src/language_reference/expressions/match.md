# `match`-expression


## Syntax
> `match` [expression](./expressions.md) `{`
>     [pattern](../pattern.md) `=>` [expression](../expressions.md) `,`
>     ...
> `}`

The `match`-expression is used to select a value based on the value of a single
expression. It is similar to `case` statements in many languages, but supports
`pattern`-matching which allows you to bind sub-values to variables. Typically, match
statements are used on [`enum`](../type_system/enums.md) values:

```spade
enum MaybeByte {
    Some{value: uint<8>},
    None
}

fn byte_or_zero(in: MaybeByte) -> uint<8> {
    match in {
        // Bind the inner value to a new variable and return it
        MaybeByte::Some(value) => value,
        MaybeByte::None => 0,
    }
}
```
but they can also be used on any values

If more than one pattern matches the value, the first pattern will be selected.

A match statement must cover all possible values of the matched expression. If
this is not the case, the compiler emits an error.





