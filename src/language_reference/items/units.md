# Units

Units are the basic building blocks of a Spade project, they correspond to
modules in Verilog, and entities in VHDL. Units come in three flavors:
functions, pipelines and entities.

## Functions

Functions are combinational circuits (or pure, in software terms), that is they
have no internal state, and can not read or set mutable wires.

## Pipelines

Pipelines have a specified delay between input and output, and have explicit staging
statements.

## Entities

Finally, entities are the most general units, they can have state, and the
input-output delay is arbitrary. They therefore have roughly the same
programming model as VHDL and Verilog.

