# Refutability

A pattern which matches all values of its type is *irrefutable* while one which
only matches conditionally is *refutable*.

For example, a pattern unpacking a tuple is *irrefutable* because all values of type `(T, Y)` will
match `(a, b)`
```spade
let tuple: (T, Y) = ...;
match tuple {
    (a, b) => {...}
}
```
while one which matches an enum variant is *refutable* because the `None` option will
not match
```spade
enum Option<T> {
    Some{val: T},
    None,
}
match x {
    Some(x) => {...} // refutable: None not covered
    ...
}
```


