# Generics

In a lot of cases, you want code to be generic over many different types,
therefore both types and [units](../units.md) support generic parameters.

## Defining generics

Units and types which are generic have their generic parameters specified inside
angle brackets (`<>`) after the name. The generics can be either integers
denoted by `#`, or types which do not have `#` sign. In the body of the generic
[item](../items.md), the generic types are referred to by their names

For example a struct storing an array of arbitrary length and type is defined as
```spade
struct ContainsArray<T, #N> {
    inner: [T, N]
}
```

## Using generics

When specifying generic parameters, angle brackets (`<>`) are also used. For example, a function
which takes a `ContainsArray` with 5 8-bit integers is defined as

```spade
fn takes_array(a: ContainsArray<int<8>, 5>) {
    ...
}
```
