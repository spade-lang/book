# Pipelines

Pipelines are an important construct in most hardware designs, and one of the key unique features of Spade is its native support for pipelining. 

Like the [blinky](https://docs.spade-lang.org/blinky.html) chapter, this one has two versions:

- [Pipelines (for software people)](./pipelines_sw.md) for software developers, which introduces what pipelines are in addition to how they are expressed in Spade.
- [Pipelines (for hardware people)](./pipelines_hw.md) for people who are already familiar with hardware design and how pipelines work.
