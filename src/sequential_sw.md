# Sequential Hardware (for Software People)

So far, outside the blinky chapter we have only seen circuits that don't have
any internal state. They take inputs and produce corresponding outputs without
any apparent delay. In software terms, we have been focusing on pure functions
and operations. But almost all interesting circuits are more complex and
require some internal state. As discussed in the blinky chapter, we can 
use registers to store values in order to make our circuits stateful.

In the blinky chapter, we also introduced a software analogy where we modelled
circuits as functions that are executed at a fixed rate and maintain state
through global variables. For example, we modelled our blinky like this:
```python
COUNTER = 0
def blinky():
  global COUNTER
  if COUNTER == 1000:
    COUNTER = 0
  else:
    COUNTER = COUNTER + 1
  # The LED should be on in the second counter interval
  return COUNTER > 500

import time

while True:
  start = time.time()
  print(blinky())
  end = time.time()
  time.sleep(0.001 - (end - start))
```

Like any analogy, this one is flawed but can still be used for understanding, and in this
chapter we will try to build some more intuition for how Spade and hardware behave using this analogy. First though, let's look at two differences between this analogy and the Spade we've seen so far:

## Registers are Not Global


