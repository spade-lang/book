# Swim

<!-- image of pheobe swimming through code libraries or something needed -->

Swim is a batteries-included build system and package manager for the Spade programming language. 
It manages rebuilds of Spade source code, the compiler and any additional Verilog, supports simulation using your favorite simulators (icarus, verilator), and automates synthesis for ECP5 and iCE40 and gowin using [yosys](https://github.com/YosysHQ/yosys) and [nextpnr](https://github.com/YosysHQ/nextpnr). 
The generated Verilog can also, of course, be used with any other tool.

Learn how to:

- [Install Swim](./install.md)
- [Use Swim](./usage.md)
