## Installing Swim

Swim can be installed using the Rust package manager, `cargo`.
To install Rust and `cargo`, follow the instructions at [rustup.rs](rustup.rs).

Once `cargo` is installed, you can install the latest development version of Swim by
running:
```shell
cargo install --git https://gitlab.com/spade-lang/swim
```

Remember to add the `~/.cargo/bin/` directory to your `PATH` if you haven't already.

If you want to use the simulation and "place and route" features you will also need a synthesis tool like [yosys](https://github.com/YosysHQ/yosys).
