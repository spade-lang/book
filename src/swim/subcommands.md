# Community Subcommands

Swim, like [cargo](https://github.com/rust-lang/cargo/wiki/Third-party-cargo-subcommands), supports extending its functionality with community-defined subcommands.
Feel free to add your own subcommand to this list!

| Name | Description |
| --- | --- | 
| [`swim-clean-all`](https://github.com/ethanuppal/swim-clean-all) | Inspired by cargo-clean-all: recursively clean swim projects |
