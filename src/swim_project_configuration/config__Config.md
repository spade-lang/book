<div class=tomldoc>

<details open>

<summary id="Config">Config</summary>

The main project configuration specified in `swim.toml`

## Summary
```toml
# The name of the library. Must be a valid Spade identifier
# Anything defined in this library will be under the `name` namespace
name = "…"
# List of optimization passes to apply in the Spade compiler. The passes are applied
# in the order specified here. Additional passes specified on individual modules with
# #[optimize(...)] are applied before global passes.
optimizations = ["…", …]
# List of commands to run before anything else.
preprocessing = ["…", …] # Optional
# Map of libraries to include in the build.
# 
# Example:
# ```toml
# [libraries]
# protocols = {git = https://gitlab.com/TheZoq2/spade_protocols.git}
# spade_v = {path = "deps/spade-v"}
# ```
libraries = {key: <Library>, …} # Optional
# Plugins to load. Specifies the location as a library, as well
# as arguments to the plugin
# 
# Example:
# ```toml
# [plugins.loader_generator]
# path = "../plugins/loader_generator/"
# args.asm_file = "asm/blinky.asm"
# args.template_file = "../templates/program_loader.spade"
# args.target_file = "src/programs/blinky_loader.spade"
# 
# [plugins.flamegraph]
# git = "https://gitlab.com/TheZoq2/yosys_flamegraph"
# ```
# 
# Plugins contain a `swim_plugin.toml` which describes their behaviour.
# See [crate::plugin::config::PluginConfig] for details
plugins = {key: <Plugin>, …} # Optional

# Where to find the Spade compiler. See [Library] for details
[compiler]
<Library>

# Verilog to import in both simulation and synthesis.
[verilog] # Optional
<ImportVerilog>

[simulation]
<Simulation>

[synthesis] # Optional
<Synthesis>

# Preset board configuration which can be used instead of synthesis, pnr, packing and upload
[board] # Optional
<Board>

[pnr] # Optional
<Pnr>

[packing] # Optional
<PackingTool>

[upload] # Optional
<UploadTool>

[log_output]
<LogOutputLevel>
```
<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>name</span> <span class=tomldoc_type> String </span></h3>

The name of the library. Must be a valid Spade identifier
Anything defined in this library will be under the `name` namespace

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>optimizations</span> <span class=tomldoc_type> [String] </span></h3>

List of optimization passes to apply in the Spade compiler. The passes are applied
in the order specified here. Additional passes specified on individual modules with
#[optimize(...)] are applied before global passes.

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>compiler</span> <span class=tomldoc_type> <a href="#Library">Library</a> </span></h3>

Where to find the Spade compiler. See [Library] for details

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>preprocessing</span> <span class=tomldoc_type> [String] </span></h3>

List of commands to run before anything else.

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>verilog</span> <span class=tomldoc_type> <a href="#ImportVerilog">ImportVerilog</a> </span></h3>

Verilog to import in both simulation and synthesis.

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>simulation</span> <span class=tomldoc_type> <a href="#Simulation">Simulation</a> </span></h3>


</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>synthesis</span> <span class=tomldoc_type> <a href="#Synthesis">Synthesis</a> </span></h3>


</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>board</span> <span class=tomldoc_type> <a href="#Board">Board</a> </span></h3>

Preset board configuration which can be used instead of synthesis, pnr, packing and upload

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>pnr</span> <span class=tomldoc_type> <a href="#Pnr">Pnr</a> </span></h3>


</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>packing</span> <span class=tomldoc_type> <a href="#PackingTool">PackingTool</a> </span></h3>


</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>upload</span> <span class=tomldoc_type> <a href="#UploadTool">UploadTool</a> </span></h3>


</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>libraries</span> <span class=tomldoc_type> Map[String =&gt; <a href="#Library">Library</a>] </span></h3>

Map of libraries to include in the build.

Example:
```toml
[libraries]
protocols = {git = https://gitlab.com/TheZoq2/spade_protocols.git}
spade_v = {path = "deps/spade-v"}
```

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>plugins</span> <span class=tomldoc_type> Map[String =&gt; <a href="#Plugin">Plugin</a>] </span></h3>

Plugins to load. Specifies the location as a library, as well
as arguments to the plugin

Example:
```toml
[plugins.loader_generator]
path = "../plugins/loader_generator/"
args.asm_file = "asm/blinky.asm"
args.template_file = "../templates/program_loader.spade"
args.target_file = "src/programs/blinky_loader.spade"

[plugins.flamegraph]
git = "https://gitlab.com/TheZoq2/yosys_flamegraph"
```

Plugins contain a `swim_plugin.toml` which describes their behaviour.
See [crate::plugin::config::PluginConfig] for details

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>log_output</span> <span class=tomldoc_type> <a href="#LogOutputLevel">LogOutputLevel</a> </span></h3>


</div>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="LogOutputLevel">LogOutputLevel</summary>


### One of these strings:
- `"Full"`
- `"Minimal"`

</details>

</div>
<div class=tomldoc>

<details >

<summary id="Plugin">Plugin</summary>


## Summary
```toml
args = {key: "…", …}

[lib]
<Library>
```
<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>lib</span> <span class=tomldoc_type> <a href="#Library">Library</a> </span></h3>


</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>args</span> <span class=tomldoc_type> Map[String =&gt; String] </span></h3>


</div>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="UploadTool">UploadTool</summary>


### One of the following:
#### icesprog
```toml
tool = "icesprog"

```
<details class=enum_field_docs>

<summary>Fields</summary>



</details>

#### iceprog
```toml
tool = "iceprog"

```
<details class=enum_field_docs>

<summary>Fields</summary>



</details>

#### tinyprog
```toml
tool = "tinyprog"

```
<details class=enum_field_docs>

<summary>Fields</summary>



</details>

#### openocd
```toml
tool = "openocd"
config_file = "path/to/file"
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>config_file</span> <span class=tomldoc_type> FilePath </span></h4>


</div>


</details>

#### fujprog
```toml
tool = "fujprog"

```
<details class=enum_field_docs>

<summary>Fields</summary>



</details>

#### openFPGALoader
```toml
tool = "openFPGALoader"
board = "…"
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>board</span> <span class=tomldoc_type> String </span></h4>


</div>


</details>

#### custom
Instead of running a pre-defined set of commands to upload, run the specified
list of commands in a shell. #packing_result# will be replaced by the packing
output
```toml
tool = "custom"
commands = ["…", …]
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>commands</span> <span class=tomldoc_type> [String] </span></h4>


</div>


</details>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="PackingTool">PackingTool</summary>


### One of the following:
#### icepack
```toml
tool = "icepack"

```
<details class=enum_field_docs>

<summary>Fields</summary>



</details>

#### ecppack
```toml
tool = "ecppack"
idcode = "…" # Optional
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>idcode</span> <span class=tomldoc_type> String </span></h4>


</div>


</details>

#### gowin_pack
```toml
tool = "gowin_pack"
device = "…"
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>device</span> <span class=tomldoc_type> String </span></h4>


</div>


</details>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="Pnr">Pnr</summary>


### One of the following:
#### ice40
```toml
architecture = "ice40"

[device_args]
<Ice40Args>
# If set, inputs and outputs of the top module do not need a corresponding field
# in the pin file. This is helpful for benchmarking when pin mapping is irreleveant, but
# when running in hardware, it is recommended to leave this off in order to get a warning
# when pins aren't set in the pin file.
allow_unconstrained = true|false
# Continue to the upload step even if the timing isn't met.
# This is helpful when you suspect that the place-and-route tool is conservative
# with its timing requirements, but gives no guarantees about correctness.
allow_timing_fail = true|false
# The path to a file which maps inputs and outputs of your top module to physical pins.
# On ECP5 chips, this is a `pcf` file, and on iCE40, it is an `lpf` file.
pin_file = "path/to/file"
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>device_args</span> <span class=tomldoc_type> <a href="#Ice40Args">Ice40Args</a> </span></h4>


</div>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>allow_unconstrained</span> <span class=tomldoc_type> bool </span></h4>

If set, inputs and outputs of the top module do not need a corresponding field
in the pin file. This is helpful for benchmarking when pin mapping is irreleveant, but
when running in hardware, it is recommended to leave this off in order to get a warning
when pins aren't set in the pin file.

</div>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>allow_timing_fail</span> <span class=tomldoc_type> bool </span></h4>

Continue to the upload step even if the timing isn't met.
This is helpful when you suspect that the place-and-route tool is conservative
with its timing requirements, but gives no guarantees about correctness.

</div>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>pin_file</span> <span class=tomldoc_type> FilePath </span></h4>

The path to a file which maps inputs and outputs of your top module to physical pins.
On ECP5 chips, this is a `pcf` file, and on iCE40, it is an `lpf` file.

</div>


</details>

#### ecp5
```toml
architecture = "ecp5"

[device_args]
<Ecp5Args>
# If set, inputs and outputs of the top module do not need a corresponding field
# in the pin file. This is helpful for benchmarking when pin mapping is irreleveant, but
# when running in hardware, it is recommended to leave this off in order to get a warning
# when pins aren't set in the pin file.
allow_unconstrained = true|false
# Continue to the upload step even if the timing isn't met.
# This is helpful when you suspect that the place-and-route tool is conservative
# with its timing requirements, but gives no guarantees about correctness.
allow_timing_fail = true|false
# The path to a file which maps inputs and outputs of your top module to physical pins.
# On ECP5 chips, this is a `pcf` file, and on iCE40, it is an `lpf` file.
pin_file = "path/to/file"
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>device_args</span> <span class=tomldoc_type> <a href="#Ecp5Args">Ecp5Args</a> </span></h4>


</div>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>allow_unconstrained</span> <span class=tomldoc_type> bool </span></h4>

If set, inputs and outputs of the top module do not need a corresponding field
in the pin file. This is helpful for benchmarking when pin mapping is irreleveant, but
when running in hardware, it is recommended to leave this off in order to get a warning
when pins aren't set in the pin file.

</div>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>allow_timing_fail</span> <span class=tomldoc_type> bool </span></h4>

Continue to the upload step even if the timing isn't met.
This is helpful when you suspect that the place-and-route tool is conservative
with its timing requirements, but gives no guarantees about correctness.

</div>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>pin_file</span> <span class=tomldoc_type> FilePath </span></h4>

The path to a file which maps inputs and outputs of your top module to physical pins.
On ECP5 chips, this is a `pcf` file, and on iCE40, it is an `lpf` file.

</div>


</details>

#### gowin
```toml
architecture = "gowin"

[device_args]
<GowinArgs>
# If set, inputs and outputs of the top module do not need a corresponding field
# in the pin file. This is helpful for benchmarking when pin mapping is irreleveant, but
# when running in hardware, it is recommended to leave this off in order to get a warning
# when pins aren't set in the pin file.
allow_unconstrained = true|false
# Continue to the upload step even if the timing isn't met.
# This is helpful when you suspect that the place-and-route tool is conservative
# with its timing requirements, but gives no guarantees about correctness.
allow_timing_fail = true|false
# The path to a file which maps inputs and outputs of your top module to physical pins.
# On ECP5 chips, this is a `pcf` file, and on iCE40, it is an `lpf` file.
pin_file = "path/to/file"
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>device_args</span> <span class=tomldoc_type> <a href="#GowinArgs">GowinArgs</a> </span></h4>


</div>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>allow_unconstrained</span> <span class=tomldoc_type> bool </span></h4>

If set, inputs and outputs of the top module do not need a corresponding field
in the pin file. This is helpful for benchmarking when pin mapping is irreleveant, but
when running in hardware, it is recommended to leave this off in order to get a warning
when pins aren't set in the pin file.

</div>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>allow_timing_fail</span> <span class=tomldoc_type> bool </span></h4>

Continue to the upload step even if the timing isn't met.
This is helpful when you suspect that the place-and-route tool is conservative
with its timing requirements, but gives no guarantees about correctness.

</div>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>pin_file</span> <span class=tomldoc_type> FilePath </span></h4>

The path to a file which maps inputs and outputs of your top module to physical pins.
On ECP5 chips, this is a `pcf` file, and on iCE40, it is an `lpf` file.

</div>


</details>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="GowinArgs">GowinArgs</summary>


## Summary
```toml

[device]
<GowinDevice>
```
<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>device</span> <span class=tomldoc_type> <a href="#GowinDevice">GowinDevice</a> </span></h3>


</div>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="GowinDevice">GowinDevice</summary>


### One of these strings:
- `"GW1NR-UV9QN881C6/I5"`
- `"GW1N-LV1QN48C6/I5"`
- `"GW1NZ-LV1QN48C6/I5"`
- `"GW1NSR-LV4CQN48PC7/I6"`
- `"GW1NR-LV9QN88PC6/I5"`
- `"GW2AR-LV18QN88C8/I7"`
- `"GW2A-LV18PG256C8/I7"`
- `"GW1N-UV4LQ144C6/I5"`
- `"GW1NS-UX2CQN48C5/I4"`
- `"GW1NR-LV9LQ144PC6/I5"`

</details>

</div>
<div class=tomldoc>

<details >

<summary id="Lv18QNFamily">Lv18QNFamily</summary>


### One of these strings:
- `"GW2A-18C"`
- `"GW2AR-18C"`
- `"GW2ANR-18C"`
- `"String"`
    Specify a raw string for the family instead of the swim-provided families. Used
    if swim doesn't support the configuration you want.

</details>

</div>
<div class=tomldoc>

<details >

<summary id="Lv9QN88PC6Family">Lv9QN88PC6Family</summary>


### One of these strings:
- `"GW1N-9C"`
- `"String"`
    Specify a raw string for the family instead of the swim-provided families. Used
    if swim doesn't support the configuration you want.

</details>

</div>
<div class=tomldoc>

<details >

<summary id="Lv1qn48c6I5Family">Lv1qn48c6I5Family</summary>


### One of these strings:
- `"GW1NZ-1"`
- `"String"`
    Specify a raw string for the family instead of the swim-provided families. Used
    if swim doesn't support the configuration you want.

</details>

</div>
<div class=tomldoc>

<details >

<summary id="Ecp5Args">Ecp5Args</summary>


## Summary
```toml
package = "…"

[device]
<Ecp5Device>
```
<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>package</span> <span class=tomldoc_type> String </span></h3>


</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>device</span> <span class=tomldoc_type> <a href="#Ecp5Device">Ecp5Device</a> </span></h3>


</div>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="Ecp5Device">Ecp5Device</summary>


### One of these strings:
- `"LFE5U-12F"`
- `"LFE5U-25F"`
- `"LFE5U-45F"`
- `"LFE5U-85F"`
- `"LFE5UM-25F"`
- `"LFE5UM-45F"`
- `"LFE5UM-85F"`
- `"LFE5UM5G-25F"`
- `"LFE5UM5G-45F"`
- `"LFE5UM5G-85F"`

</details>

</div>
<div class=tomldoc>

<details >

<summary id="Ice40Args">Ice40Args</summary>


## Summary
```toml
package = "…"

[device]
<Ice40Device>
```
<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>package</span> <span class=tomldoc_type> String </span></h3>


</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>device</span> <span class=tomldoc_type> <a href="#Ice40Device">Ice40Device</a> </span></h3>


</div>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="Ice40Device">Ice40Device</summary>


### One of these strings:
- `"iCE40LP384"`
- `"iCE40LP1K"`
- `"iCE40LP4K"`
- `"iCE40LP8K"`
- `"iCE40HX1K"`
- `"iCE40HX4K"`
- `"iCE40HX8K"`
- `"iCE40UP3K"`
- `"iCE40UP5K"`
- `"iCE5LP1K"`
- `"iCE5LP2K"`
- `"iCE5LP4K"`

</details>

</div>
<div class=tomldoc>

<details >

<summary id="Board">Board</summary>


### One of the following:
#### Ecpix5
```toml
name = "Ecpix5"
pin_file = "path/to/file" # Optional
config_file = "path/to/file" # Optional
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>pin_file</span> <span class=tomldoc_type> FilePath </span></h4>


</div>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>config_file</span> <span class=tomldoc_type> FilePath </span></h4>


</div>


</details>

#### GoBoard
```toml
name = "GoBoard"
pcf = "path/to/file" # Optional
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>pcf</span> <span class=tomldoc_type> FilePath </span></h4>


</div>


</details>

#### tinyfpga-bx
```toml
name = "tinyfpga-bx"
pcf = "path/to/file" # Optional
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>pcf</span> <span class=tomldoc_type> FilePath </span></h4>


</div>


</details>

#### Icestick
```toml
name = "Icestick"
pcf = "path/to/file" # Optional
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>pcf</span> <span class=tomldoc_type> FilePath </span></h4>


</div>


</details>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="Synthesis">Synthesis</summary>


## Summary
```toml
# The name of the unit to use as a top module for the design. The name must
# be an absolute path to the unit, for example `proj::main::top`, unless the
# module is marked `#[no_mangle]` in which case the name is used.
# 
# Can also be set to the name of a module defined in verilog if a pure verilog top
# is desired.
top = "…"
# The yosys command to use for synthesis
command = "…"

# Extra verilog files only needed during the synthesis process.
[verilog] # Optional
<ImportVerilog>
```
<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>top</span> <span class=tomldoc_type> String </span></h3>

The name of the unit to use as a top module for the design. The name must
be an absolute path to the unit, for example `proj::main::top`, unless the
module is marked `#[no_mangle]` in which case the name is used.

Can also be set to the name of a module defined in verilog if a pure verilog top
is desired.

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>command</span> <span class=tomldoc_type> String </span></h3>

The yosys command to use for synthesis

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>verilog</span> <span class=tomldoc_type> <a href="#ImportVerilog">ImportVerilog</a> </span></h3>

Extra verilog files only needed during the synthesis process.

</div>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="Simulation">Simulation</summary>


## Summary
```toml
# Directory containing all test benches
testbench_dir = "path/to/file"
# Extra dependencies to install to the test venv via pip
python_deps = ["…", …] # Optional
# The simulator to use as the cocotb backend. Currently verified to support verilator and
# icarus, but other simulators supported by cocotb may also work.
# 
# Defaults to 'icarus'
# 
# Requires a relatively recent version of verilator
simulator = "…"
# The C++ version to use when compiling verilator test benches. Anything that
# clang or gcc accepts in the -std= field works, but the verilator wrapper requires
# at least c++17.
# Defaults to c++17
cpp_version = "…" # Optional
# Extra arguments to pass to verilator when building C++ test benches. Supports substituting
# `#ROOT_DIR#` to get project-relative directories
verilator_args = ["…", …] # Optional
```
<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>testbench_dir</span> <span class=tomldoc_type> FilePath </span></h3>

Directory containing all test benches

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>python_deps</span> <span class=tomldoc_type> [String] </span></h3>

Extra dependencies to install to the test venv via pip

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>simulator</span> <span class=tomldoc_type> String </span></h3>

The simulator to use as the cocotb backend. Currently verified to support verilator and
icarus, but other simulators supported by cocotb may also work.

Defaults to 'icarus'

Requires a relatively recent version of verilator

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>cpp_version</span> <span class=tomldoc_type> String </span></h3>

The C++ version to use when compiling verilator test benches. Anything that
clang or gcc accepts in the -std= field works, but the verilator wrapper requires
at least c++17.
Defaults to c++17

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>verilator_args</span> <span class=tomldoc_type> [String] </span></h3>

Extra arguments to pass to verilator when building C++ test benches. Supports substituting
`#ROOT_DIR#` to get project-relative directories

</div>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="ImportVerilog">ImportVerilog</summary>


## Summary
```toml
# Search paths for Verilog include directives.
include = ["path/to/file", …] # Optional
# Paths to Verilog files to import. Supports glob syntax.
sources = ["…", …] # Optional
```
<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>include</span> <span class=tomldoc_type> [FilePath] </span></h3>

Search paths for Verilog include directives.

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>sources</span> <span class=tomldoc_type> [String] </span></h3>

Paths to Verilog files to import. Supports glob syntax.

</div>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="Library">Library</summary>

Location of a library or external code. Either a link to a git repository, or
a path relative to the root of the project.

```toml
compiler = {git = "https://gitlab.com/spade-lang/spade/"}
```

```toml
path = "compiler/"
```

### One of the following:
#### Git
Downloaded from git and managed by swim
```toml

git = "…"
commit = "…" # Optional
tag = "…" # Optional
branch = "…" # Optional
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>git</span> <span class=tomldoc_type> String </span></h4>


</div>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>commit</span> <span class=tomldoc_type> String </span></h4>


</div>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>tag</span> <span class=tomldoc_type> String </span></h4>


</div>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>branch</span> <span class=tomldoc_type> String </span></h4>


</div>


</details>

#### Path
A library at the specified path. The path is relative to `swim.toml`
```toml

path = "path/to/file"
```
<details class=enum_field_docs>

<summary>Fields</summary>

<div class=field_doc>

<h4 class=enum_field> <span class=tomldoc_param_name>path</span> <span class=tomldoc_type> FilePath </span></h4>


</div>


</details>


</details>

</div>