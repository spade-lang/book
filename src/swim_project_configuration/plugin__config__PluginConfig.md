<div class=tomldoc>

<details open>

<summary id="PluginConfig">PluginConfig</summary>


## Summary
```toml
# True if this plugin needs the CXX bindings for the Spade compiler to be built
requires_cxx = true|false
# Commands required to build the plugin. Run before any project compilation steps
build_commands = ["…", …]
# The files which this plugin produces
builds = [<BuildResult>, …]
# Arguments which must be set in the `swim.toml` of projects using the plugin
required_args = ["…", …]
# Commands to run after building swim file but before anything else
post_build_commands = ["…", …]
# Commands which the user can execute
commands = {key: <PluginCommand>, …}

# Things to do during the synthesis process
[synthesis] # Optional
<SynthesisConfig>
```
<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>requires_cxx</span> <span class=tomldoc_type> bool </span></h3>

True if this plugin needs the CXX bindings for the Spade compiler to be built

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>build_commands</span> <span class=tomldoc_type> [String] </span></h3>

Commands required to build the plugin. Run before any project compilation steps

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>builds</span> <span class=tomldoc_type> [<a href="#BuildResult">BuildResult</a>] </span></h3>

The files which this plugin produces

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>required_args</span> <span class=tomldoc_type> Set[String] </span></h3>

Arguments which must be set in the `swim.toml` of projects using the plugin

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>post_build_commands</span> <span class=tomldoc_type> [String] </span></h3>

Commands to run after building swim file but before anything else

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>synthesis</span> <span class=tomldoc_type> <a href="#SynthesisConfig">SynthesisConfig</a> </span></h3>

Things to do during the synthesis process

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>commands</span> <span class=tomldoc_type> Map[String =&gt; <a href="#PluginCommand">PluginCommand</a>] </span></h3>

Commands which the user can execute

</div>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="PluginCommand">PluginCommand</summary>


## Summary
```toml
# List of system commands to run in order to execute the command
# 
# Commands specified by the user, i.e. whatever is after `swim plugin <command>`
# is string replaced into `%args%` in the resulting command string. The arguments
# are passed as strings, to avoid shell expansion
script = ["…", …]

# The build step after which to run this command
[after]
<BuildStep>
```
<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>script</span> <span class=tomldoc_type> [String] </span></h3>

List of system commands to run in order to execute the command

Commands specified by the user, i.e. whatever is after `swim plugin <command>`
is string replaced into `%args%` in the resulting command string. The arguments
are passed as strings, to avoid shell expansion

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>after</span> <span class=tomldoc_type> <a href="#BuildStep">BuildStep</a> </span></h3>

The build step after which to run this command

</div>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="BuildStep">BuildStep</summary>


### One of these strings:
- `"Start"`
    Before any other processing takes place
- `"SpadeBuild"`
- `"Simulation"`
- `"Synthesis"`
- `"Pnr"`
- `"Upload"`

</details>

</div>
<div class=tomldoc>

<details >

<summary id="SynthesisConfig">SynthesisConfig</summary>


## Summary
```toml
# Yosys commands to run after the normal yosys flow
yosys_post = ["…", …]
```
<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>yosys_post</span> <span class=tomldoc_type> [String] </span></h3>

Yosys commands to run after the normal yosys flow

</div>


</details>

</div>
<div class=tomldoc>

<details >

<summary id="BuildResult">BuildResult</summary>


## Summary
```toml
# The path of a file built by this build step
path = "…"

# The first build step for which this file is required. This will trigger
# a re-build of this build step if the file was changed
[needed_in]
<BuildStep>
```
<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>path</span> <span class=tomldoc_type> String </span></h3>

The path of a file built by this build step

</div>

<div class=field_doc>

<h3 class=struct_field> <span class=tomldoc_param_name>needed_in</span> <span class=tomldoc_type> <a href="#BuildStep">BuildStep</a> </span></h3>

The first build step for which this file is required. This will trigger
a re-build of this build step if the file was changed

</div>


</details>

</div>