# LED protocol overview

Now that we are familiar with the project layout, we can start writing the
driver for our LEDs. To do so, a good place to start is the
[datasheet](https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf). By reading it we can find out how the protocol works:

The LEDs are chained together, with us talking to the data in pin on the first
LED in the chain, and it relaying messages to the rest of the chain
automatically.

Data transmission consists of 3 symbols:
 - 0 code
 - 1 code
 - RET code

Each LED has 24 bit color, 8 bits per channel and the transmission order is GRB
[^color_order] with the most significant bit first. The first 24 bits of color
control the first LED, the next 24 the second and so on, until the RET code is
sent at which point data transmission re-starts from the beginning

[^color_order]: Because apparently standard color orders like RGB is too mainstream

As a more graphical example, a transmission of the color information for a sequence
of three LEDs look like this:

```
| G7..0 | R7..0 | B7..0 | G7..0 | R7..0 | B7..0 | G7..0 | R7..0 | B7..0 | RET |...
|<        LED 1        >|<        LED 2        >|<        LED 2        >|     |< ...
```

Each color segment is a sequence of `1` or `0` codes depending on the desired
color for that led and color channel.

We should also have a look at the waveform of the 0, 1 and RET codes which
look like this (see the datasheet for prettier figures):


### 0 code
```
------+
      |
      +-----------
| T0H |    T0L   |
```
I.e. a signal that is High for `T0H` units of time, followed by Low for `T0L`
units of time

### 1 code
```
----------+
          |
          +-------
|   T1H   |  T1L |
```
I.e. a signal that is High for `T1H` units of time, followed by Low for `T1L`
units of time. It is very similar to the 0 code, but for the 1 code, the high
duration is longer than the low duration.

### RET code

The RET code is just a Low signal which lasts for `Tret`  units of time.

> **NOTE**: The datasheet usually refers to this signal as `reset` and the
> timing as `Treset`. In order to make the rest of this text less confusing, we
> use the name `ret` throughout, as we already have a FPGA reset signal in our
> design which has different purposes.

### Durations

We'll leave the durations of the signals for now and get back to them when we
start implementing things. If you're curious already, have a look at the
datasheet.

With the discussion of the external protocol out of the way, the next section
will discuss our internal protocol, i.e. what interface we expose to users of
our driver.
